//
//  main.m
//  Lesson1HomeTasks
//
//  Created by Vladimir on 27.03.15.
//  Copyright (c) 2015 Vladimir. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
